import logging
import os
from flask import Flask
from flask_debugtoolbar import DebugToolbarExtension
#Custom functions
from APIBase.utils.extensions import db
from APIBase.lib.config import get_config
from APIBase.lib.restplus import apiBlueprint,api
from blueprints.oauthServer.routes import oauth
from blueprints.oauthServer.oauth2 import config_oauth
from blueprints.api.routes import ns as dnacAPI
from blueprints.webhooks.routes import ns as teamwebhook

runOnce=False

 
def create_app():
    # Setup logging

    if os.environ['RUN_STATE'] == 'DEV':
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        logging.basicConfig(filename='../api-server.log', level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s', )
        logging.debug('Local Development Logging Enabled')
    else:
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)
        logging.basicConfig(filename='/opt/logs/api-server.log',
                            level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',)
        logging.debug('Local Production Logging Enabled')


    appconfig = get_config()
    application = Flask(__name__)
    application.config.from_object(appconfig)
    api.add_namespace(dnacAPI)
    api.add_namespace(teamwebhook)
    config_oauth(application)
    application.register_blueprint(oauth)
    application.register_blueprint(apiBlueprint)
    db.init_app(application)
    db.app = application


    return application

app=create_app()
if __name__ == '__main__':
   #db.create_all()
   #application=create_app()
   if runOnce == True:
       #db.create_all()
       print('done')
        #application.run ()
