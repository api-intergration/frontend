from celery import Celery
from .celeryconfig import  constants
def init_celery(app):
	"""
	
	Args:
		app:

	Returns:

	"""
	celery = Celery()
	celery.conf.update(
		{
			'broker_url':        constants.CELERY_BROKER_URL,
			'result_backend':    constants.CELERY_RESULT_BACKEND,
			'result_serializer': constants.RESULT_SERIALIZER,
			'accept_content':    constants.CELERY_ACCEPT_CONTENT,
		}
	)

	class ContextTask(celery.Task):
		def __call__(self, *args, **kwargs):
			with app.app_context():
				return self.run(*args, **kwargs)

	celery.Task = ContextTask
	return celery



