FROM registry.gitlab.com/api-intergration/server-settings/base:latest as Portal
ARG SYSTEM
ARG PORTAL
ARG SHARED
ARG TASKSETUP
ARG TOKEN

ARG DNACUTILS
ENV TOKEN=$TOKEN
ENV PORTAL=$PORTAL
ENV SHARED=$SHARED
ENV TASKSETUP=$TASKSETUP
ENV SYSTEM=$SYSTEM

#ADD https://gitlab.com/Josh.Lipton/dnac_intergrator/-/archive/master/dnac_intergrator-master.tar.gz /opt/app/master.tar.gz
#RUN tar -zvxf /opt/app/master.tar.gz -C /opt/app/ --strip-components 1
#RUN pip install git+https://__token__:$TOKEN@gitlab.com/api-intergration/utils/shared.git

RUN mkdir /opt/configs/uwsig
RUN curl --header "PRIVATE-TOKEN: $TOKEN" -o /opt/configs/uwsig/uwsig.ini https://gitlab.com/api/v4/projects/$SYSTEM/repository/files/uwsig%2Fuwsig.ini/raw?ref=master

WORKDIR /opt/system
RUN curl --header "PRIVATE-TOKEN: $TOKEN" -o /opt/system/start.sh https://gitlab.com/api/v4/projects/$SYSTEM/repository/files/start.sh/raw?ref=master
RUN chmod +x /opt/system/start.sh

WORKDIR /opt/app/
RUN curl --header "PRIVATE-TOKEN: $TOKEN"  https://gitlab.com/api/v4/projects/$PORTAL/repository/archive.tar.gz -O
RUN tar -zxvf /opt/app/archive.tar.gz -C /opt/app --strip-components 1

#RUN rm -R /opt/app/
#USER www-data
WORKDIR /opt/app
#CMD dnacsync tasks start




