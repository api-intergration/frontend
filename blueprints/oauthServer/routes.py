from flask import Blueprint, request, session, url_for
from flask import render_template, redirect, jsonify
from werkzeug.security import gen_salt
from authlib.integrations.flask_oauth2 import current_token
from authlib.oauth2.rfc6749 import OAuth2Error
from APIBase.models.models import  User, OAuth2Client
from APIBase.utils.extensions import db
from .oauth2 import authorization, require_oauth
import logging

oauth = Blueprint('oauth',__name__, template_folder='templates',url_prefix='/oauth')

def current_user():
	if 'id' in session:
		uid = session['id']
		return User.query.get (uid)
	return None


@oauth.route ('/', methods=('GET', 'POST'))
def oauth_home():
	if request.method == 'POST':
		username = request.form.get ('username')
		user = User.query.filter_by (username=username).first ()
		if not user:
			user = User (username=username)
			db.session.add (user)
			logging.debug('User Name Created' + str(user))
			db.session.commit ()
		logging.debug('User Id for Session' + str (user.id))
		session['id'] = user.id
		return redirect (url_for('oauth.oauth_home'))
	user = current_user ()
	if user:
		clients = OAuth2Client.query.filter_by (user_id=user.id).all ()
	else:
		clients = []
	return render_template ('oAuthServer/home.html', user=user, clients=clients)


@oauth.route ('/logout')
def oauth_logout():
	del session['id']
	return redirect (url_for('oauth.oauth_home'))


@oauth.route ('/create_client', methods=('GET', 'POST'))
def oauth_create_client():
	user = current_user ()
	if not user:
		return redirect ('/')
	if request.method == 'GET':
		return render_template ('oAuthServer/create_client.html')
	client = OAuth2Client (**request.form.to_dict (flat=True))
	client.user_id = user.id
	client.client_id = gen_salt (24)
	if client.token_endpoint_auth_method == 'none':
		client.client_secret = ''
	else:
		client.client_secret = gen_salt (48)
	db.session.add (client)
	db.session.commit ()
	return redirect (url_for('oauth.oauth_home'))


@oauth.route ('/authorize', methods=['GET', 'POST'])
def oauth_authorize():
	'''
	user = current_user ()
	if request.method == 'GET':
		try:
			grant = authorization.validate_consent_request (end_user=user)
			info = grant.client
			oargs = {'client_id': info.client_id,
					 'response_type': request.values['response_type'],
					 'scope': request.values['scope'],
					 'redirect_uri': request.values['redirect_uri']}
			oclientSec = {'client_secret': info.client_secret}
		except OAuth2Error as error:
			return error.error
		return render_template ('oAuthServer/authorize.html', user=user, grant=grant)
	if not user and 'username' in request.form:
		username = request.form.get ('username')
		user = User.query.filter_by (username=username).first ()
	if request.form['confirm']:
		grant_user = user
	else:
		grant_user = None

	return authorization.create_authorization_response (grant_user=grant_user)
	# form = ReusableForm (request.form, amazonAskID='123')
	# return render_template ('oAuthServer/index2.html', form=form)
	
	'''
	logging.debug('.....Oauth Authorization Funcation...........')
	global oargs
	global oclientSec
	try:
		grant = authorization.validate_consent_request (end_user=None)
	except OAuth2Error as error:
		return error.error
	info = grant.client
	user='changeme'
	
	oargs = {'client_id': info.client_id,
				'response_type': request.values['response_type'],
				'scope': request.values['scope'],
				'redirect_uri': request.values['redirect_uri'],
			    'state': request.values['state']}
	oclientSec = {'client_secret': info.client_secret}
	logging.debug((str(oargs)))
	url = url_for ('tokenLinking.tokenLinking_index', alexaID=user)
	logging.debug(str(url))
	return redirect (url_for ('tokenLinking.tokenLinking_index', alexaID=user))
	

# return alexaAPI_index ('123321')

@oauth.route ('/code/<alexaid>', methods=['GET', 'POST'])
def oauth_code(alexaid):
	logging.debug(str(alexaid))
	user = User.query.filter_by (username=alexaid).first ()
	logging.debug (str (user))
	if not user:
		user = User (username=alexaid)
		db.session.add (user)
		db.session.commit ()
	user = User.query.filter_by (username=alexaid).first ()
	grant = authorization.validate_consent_request (end_user=user)
	return authorization.create_authorization_response (grant_user=user)


@oauth.route ('/token', methods=['POST'])
def oauth_issue_token():
	return authorization.create_token_response ()


@oauth.route ('/revoke', methods=['POST'])
def oauth_revoke_token():
	return authorization.create_endpoint_response ('revocation')


@oauth.route ('/api/me')
@require_oauth ('all')
def oauth_api_me():
	user = current_token.user
	return jsonify (id=user.id, username=user.username)
