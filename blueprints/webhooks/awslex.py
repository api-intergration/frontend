import boto3

def sendMsgToLex(msg,userId):
	client = boto3.client('lex-runtime')
	response = client.post_text(botName='Gizmodo',botAlias='dev',userId=userId,inputText=msg)
	return response