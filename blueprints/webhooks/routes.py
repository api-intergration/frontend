from __future__ import absolute_import
import json

from flask import request,current_app
from flask_restplus import Resource
from webexteamssdk import WebexTeamsAPI, Webhook
from APIBase.lib.restplus import api
from blueprints.webhooks.awslex import sendMsgToLex
from blueprints.webhooks.function import performTask,getAttachmentInfo
from tasksetup.setup_celery import init_celery
taskserver = init_celery(current_app)
ns = api.namespace('bot',description='Webex teams bot webhook')

@ns.route('/events')
class teamsMessange(Resource):
    def post(self):
        teams = WebexTeamsAPI()
        if request.method == 'POST':
            """Respond to inbound webhook JSON HTTP POST from Webex Teams."""
        json_data = request.json
        # Create a Webhook object from the JSON data
        webhook_obj = Webhook(json_data)
        # Get the room details
        room = teams.rooms.get(webhook_obj.data.roomId)
        # Get the message details
        message = teams.messages.get(webhook_obj.data.id)
        # Get the sender's details
        person = teams.people.get(message.personId)

        print("NEW MESSAGE IN ROOM '{}'".format(room.title))
        print("FROM '{}'".format(person.displayName))
        print("MESSAGE '{}'\n".format(message.text))

        # Bot Reponse to itself prevention
        me = teams.people.me()
        if message.personId == me.id:
            # Message was sent by me (bot); do not respond.
            return "ok"
        else:
            response = sendMsgToLex(message.text,person.id)
            print(f"Intent {response['intentName']} SLOT {response['slots']}")
            task = performTask(response['intentName'],response['slots'],person,taskserver)


@ns.route('/cardResponse')
class cardResponse(Resource):
    def post(self):
        teams = WebexTeamsAPI()
        json_data = request.json
        # Create a Webhook object from the JSON data
        webhook_obj = Webhook(json_data)
        # Get the room details
        room = teams.rooms.get(webhook_obj.data.roomId)
        # Get the message details
        message = getAttachmentInfo(webhook_obj.data.id)
        message = json.loads(message.text)
        # Get the sender's details
        person = teams.people.get(message['personId'])

        print("NEW MESSAGE IN ROOM '{}'".format(room.title))
        print("FROM '{}'".format(person.displayName))
        #print("MESSAGE '{}'\n".format(message.text))
        print(f"Input: \n"
              f"{message['inputs'][0]['id'] }: {message['inputs'][0]['value']} \n"
              f"{message['inputs'][1]['id']}: {message['inputs'][1]['value']} \n")
        if message['inputs'][1]['value'] == "Approve":
            task = taskserver.send_task('DNAC:add_pnp_and_claim',
                                        queue='DNAC',
                                        args=[int(message['inputs'][0]['value']), person.emails[0]])
            print(str(message['inputs'][0]['value']))
            return ""



