
from APIBase.teamsutils.massage_help import send as respons_help
import requests,os

def performTask(function,slots,person,taskserver):

    if function =="NewPnPDevice":
        task = taskserver.send_task('DNAC:add_pnp_and_claim',
                                    queue='DNAC',
                                    args=[int(slots['deviceID']),
                                          person.emails[0]])
        print(str(slots['deviceID']))
        return task
    elif function =="GetHealth":
        task = taskserver.send_task('DNAC:overall_network_health',
                                    queue='DNAC',
                                    args=[person.emails[0]])
        return ""
    elif function == "ListAllDevices":
        task = taskserver.send_task('DNAC:list_all_devices',
                                    queue='DNAC',
                                    args=[person.emails[0]])
        return task
    elif function == "GetDeviceHealth":
        task = taskserver.send_task('DNAC:get_device_health',
                                    queue='DNAC',
                                    args=[person.emails[0],str(slots['deviceName'])])
    elif function == "ManualPnPDevice":

        task = taskserver.send_task('DNAC:manual_pnp_device',
                                    queue='DNAC',
                                    args=[person.emails[0],str(slots['deviceName'])])
    else:
        respons_help(person.emails[0])

def getAttachmentInfo(messageId):
    URL = f"https://api.ciscospark.com/v1//attachment/actions/{messageId}"
    ACCESS_TOKEN = os.environ['TEAMS_BOT_TOKEN']


    headers = {'Authorization': 'Bearer ' + ACCESS_TOKEN,
               'Content-type':  'application/json;charset=utf-8'}
    response = requests.get(URL, headers=headers)
    if response.status_code == 200:
        # Great your message was posted!
        return response
    else:
        # Oops something went wrong...  Better do something about it.
        print(response.status_code, response.text)