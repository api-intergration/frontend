from flask_restplus import fields
from APIBase.lib.restplus import api


api_device = api.model('New Device', {
	'manufacturer':fields.String(required=True, description='Device Manufacture'),
	'model_number': fields.String(required=True, description='Device Model Number'),
	'SN_sys_id': fields.String(required=False, description='Service Now or Inventory database internal id'),
	'serial' : fields.String(required=True, description='Device serial number'),
	'location': fields.String(required=True, description='Hierarchical location I.E. /Americas/California/San Jose/Building 10'),
	'name': fields.String(required=True, description='Hostname'),
	'dnac_provisioned': fields.String(readOnly=True, description='Successful DNAC Provisioned'),
	'admin_approved': fields.String(readOnly=True, description='Device approved for creation'),
})


