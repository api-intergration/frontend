from APIBase.models.datamodel import device_model
import logging

def newdevice(data):

    logging.debug(f"New device added: {str(data)}")
    try:
        newdevice = device_model(**data)
        newdevice.save()
        return newdevice
    except Exception as e:
        logging.error(f"New Device Add to DB Failed error: {str(e)}")
        return False

