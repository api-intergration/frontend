from __future__ import absolute_import
import logging
import json
from os import getenv,environ
from .api_models import api_device
from flask import request,current_app
from flask_restplus import Resource
from .apifunctions import newdevice
from APIBase.models.datamodel import device_model
from APIBase.lib.restplus import api
from tasksetup.setup_celery import init_celery
taskserver = init_celery(current_app)
ns = api.namespace('dnac', description='Workflows to interact with Cisco DNA Center')
@ns.route('/new-pnp-device')
@ns.response(201, 'Task Submitted')
class pnp(Resource):
    @ns.doc('creates a new device in DNAC PNP Databas')
    @ns.marshal_list_with(api_device)
    def post(self):
        logging.debug("API Call For POST NEW Device - /new-device")
        byPass = True
        if byPass:
            logging.debug("Running BYPAss Admin Teams Check:")
            logging.debug(
                f"BYPass Email Address: {environ['BYPASS_EMAIL']}")
            # Bypassing Teams Approval for PnP
            # person=json.loads(environ['TEAMS_USERS'])
            # logging.debug(str(person))
            device = newdevice(request.json)
            # device = device_model.find_by_serial(request.json['serial'])
            device_dict = device.serialize()
            task = taskserver.send_task('DNAC:add_pnp_and_claim',
                                        queue='DNAC',
                                        args=[int(device_dict['id']),
                                              environ['BYPASS_EMAIL']])
            print(str(device_dict['id']))
            return task
        try:
            logging.debug(f"JSON Converted: {request.json}")
            device = device_model.find_by_serial(request.json['serial'])
            
  
            if not device:
                device=newdevice(request.json)
                device_dict=device.serialize()
                task = taskserver.send_task('BOT:teams_new_device_message',queue='BOT',
                                            args=[device_dict])
                logging.debug(f"worker_teams_message task ID: {task.id}")
                return None, 201
            else:
                device_dict = device.serialize()
                task = taskserver.send_task('BOT:teams_new_device_message',
                                            queue='BOT',
                                            args=[device_dict])
                logging.debug(f"worker_teams_message task ID: {task.id}")
                return None, 201

        except Exception as e:
            logging.error(f"Error With New DEVICE: {str(e)}")
            return None, 500
    
        #print(data)

@ns.route('/overall-health/<email>')
@ns.param('email','Email Address')
@ns.doc(parms={'email': 'Email address to send health information to in teams'},
        response={202,'Task Submitted'})
@ns.response(201, 'Task Submitted')
class networkhealth(Resource):
    @ns.doc("Sends Overall Network Health to webex teams")
    def get(self,email):
        task = taskserver.send_task('DNAC:overall_network_health',
                                    queue='DNAC',
                                    args=[email])
        
        return None,201
